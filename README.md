# Credit_risk_model

## Dataset
https://www.dropbox.com/s/g578swh8valev5f/loan_data_2007_2014.csv?dl=0


## Preprocessing Files 
initial_preprocessing;
pd_datapreparation;
lgd&ead_datapreparation; 
input_data.ipynb

## Model Files 
pd.model.py; 
lgd_model.py; 
ead_model.py;
expected_loss.ipynb 





